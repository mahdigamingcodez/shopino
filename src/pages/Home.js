//#root importations
import React from 'react';

//#styling and components importation
import style from '../scss/pages/_Home.module.scss';
import Navbar from '../layout/Navbar';
import Hero from '../layout/Hero';
import CategorySlider from '../components/CategorySlider';
import Filters from '../components/Filters';
import CategoryProduct from '../layout/CategoryProduct';
import Vitrin from '../components/Vitrin';
import Attributes from '../layout/Attributes';
import Footer from '../layout/Footer';

//#images importation
import tshirt from '../images/t-shirt.png';
import beltVitrin from '../images/belt-vitrin.png';
import capVitrin from '../images/cap-vitrin.png';
import coatVitrin from '../images/coat-vitrin.png';
import hatVitrin from '../images/hat-vitrin.png';
import mantoVitrin from '../images/manto-vitrin.png';
import shoesVitrin from '../images/shoes-vitrin.png';
import sneakerVitrin from '../images/sneaker-vitrin.png';
import blackHoody from '../images/black-hoody.png';

const Home = () => {
    return (
        <div className={style.homeContainer}>
            <Navbar />
            <Hero />
            <Vitrin vitrinTitle="ویتـــــــرین" vtImageSrc1={shoesVitrin} vtImageSrc2={coatVitrin} vtImageSrc3={blackHoody} vtImageSrc4={mantoVitrin}
                vtImageSrc5={tshirt} vtImageSrc6={beltVitrin} vtImageSrc7={hatVitrin} vtImageSrc8={sneakerVitrin} vtImageSrc9={capVitrin} />
            <CategorySlider />
            <Filters />
            <CategoryProduct />
            <Attributes />
            <Footer />
        </div>
    )
}
export default Home;
