//#root importations
import React from "react";
import { Link } from "react-router-dom";

//#styling and components importation
import styles from "../scss/layout/_Footer.module.scss";

//Images and Icons
import namad1 from "../images/namad1.svg";
import namad2 from "../images/namad2.svg";
import instagram from "../images/instagram.svg";
import whatsapp from "../images/whatsapp.svg";
import telegram from "../images/telegram.svg";

const Footer = () => {
  return (
    <footer>

      <div className={styles.footerContainer}>
        <div className={styles.row1}>
          <div className={styles.aboutShop}>
            <Link to="/">
              شا<span>پ</span>ینو
            </Link>
            <p>
              لـــــورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و
              با استفاده از طـــراحان گرافیک است، چاپگرها و متون بلکه روزنامه و
              مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی
              مورد نیاز، و کــــــاربردهای متنوع با هدف بهبود ابـزارهای کاربردی
              می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینــده، شناخت
              فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری
            </p>

            <div className={styles.menu}>
              <Link to="/ex" className={styles.borderLeft}>
                قوانین ما
              </Link>
              <Link to="/ex" className={styles.borderLeft}>
                ارتباط با ما
              </Link>
              <Link to="/ex">ارسال های ما</Link>
            </div>
          </div>

          <div className={styles.aboutNamad}>
            <div className={styles.shapes}>
              <div className={styles.shapeOne}>
                <div className={styles.shapeTwo}>
                  <img src={namad1} alt="digital-namad" />
                </div>
              </div>
              <div className={styles.shapeOne}>
                <div className={styles.shapeTwo}>
                  <img src={namad2} alt="digital-namad" />
                </div>
              </div>
            </div>
            <div className={styles.socialNetworks}>
              <h3> شبکه های اجتماعی: </h3>

              <Link to="#">
                <img src={instagram} alt="instagram-icon" />
              </Link>

              <Link to="#">
                <img src={whatsapp} alt="whatsapp-icon" />
              </Link>
              <Link to="#">
                <img src={telegram} alt="telegram-icon" />
              </Link>

            </div>
          </div>
        </div>

        <div className={styles.itmabnaContainer}>
          <div className={styles.itmabna}>
            <h4>
              طراحی شده توسط تیم
              <Link to="https://itmabna.com/cgi-sys/suspendedpage.cgi">
                ITMabna
              </Link>
            </h4>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
