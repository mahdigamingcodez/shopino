//#root importation
import React from 'react';

//#styling and components importation
import style from '../scss/layout/_CategoryProduct.module.scss';
import CardsCategory from '../components/CardsCategory';
import Card from '../components/Card';

//#image importation
import tshirt from '../images/t-shirt.png';

const CategoryProduct = () => {
    return (
        <main className={style.categoryMain}>
            <section className={style.categoryProduct} id="item1">
                <CardsCategory className={style.categoryContainer} title="تی شرت" />
                <section className={style.cardsWrapper}>
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                </section>
            </section>

            <section className={style.categoryProduct} id="item2">
                <CardsCategory className={style.categoryContainer} title="هودی" />
                <section className={style.cardsWrapper}>
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                </section>
            </section>


            <section className={style.categoryProduct} id="item3">
                <CardsCategory className={style.categoryContainer} title="شلوار" />
                <section className={style.cardsWrapper}>
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                </section>
            </section>


            <section className={style.categoryProduct} id="item4">
                <CardsCategory className={style.categoryContainer} title="کفش و کتونی" />
                <section className={style.cardsWrapper}>
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                </section>
            </section>

            <section className={style.categoryProduct} id="item5">
                <CardsCategory className={style.categoryContainer} title="کلاه" />
                <section className={style.cardsWrapper}>
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                </section>
            </section>
        </main>
    );
};

export default CategoryProduct;