//#root importations
import React, { useRef } from 'react';
import { Link } from "react-router-dom";

//#styling and components importation
import style from '../scss/layout/_Navbar.module.scss';

//#importaiton source files
import searchIcon from '../images/search-icon.svg';
import saveIcon from '../images/save-icon.svg';
import shoppingCartIcon from '../images/shopping-cart-icon.svg';

const Navbar = (props) => {

    const menuSlider = useRef(null);
    const logoText = useRef(null);
    const line1 = useRef(null);
    const line2 = useRef(null);
    const line3 = useRef(null);

    const aniamtionToggle = () => {
        let menuIcon = document.querySelector("#menuIcon");
        let menuIconContainer = document.querySelector("#menuIconContainer");

        menuIcon.classList.toggle('opened');
        menuIcon.classList.contains('opened');
        menuIconContainer.classList.toggle('slider');

        if (menuIconContainer.classList.contains('slider')) {
            menuSlider.current.style.right = "0";
            logoText.current.style.color = "#fcfcfc";
            line1.current.style.stroke = "#f6d6ad";
            line2.current.style.stroke = "#f6d6ad";
            line3.current.style.stroke = "#f6d6ad";
            document.body.style.overflow = "hidden";
        } else {
            menuSlider.current.style.right = "-1024px";
            logoText.current.style.color = "#1a1c20";
            line1.current.style.stroke = "#1A1c20";
            line2.current.style.stroke = "#1A1c20";
            line3.current.style.stroke = "#1A1c20";
            document.body.style.overflow = "auto";
        }
    }

    const { navLink } = props;

    return (
        <>
            <div className={style.container}>
                <div className={style.logo}>
                    <Link to="/">شا<span>پ</span>ینو</Link>
                </div>
                <ul className={navLink}>
                    <li className={style.active}>خانه</li>
                    <li>محصولات</li>
                    <li>ارسال های ما</li>
                    <li>ارتباط با ما</li>
                </ul>
                <div className={style.icons}>
                    <img src={searchIcon} alt="Search icon" />
                    <img src={saveIcon} alt="Save icon" />
                    <img src={shoppingCartIcon} alt="shopping cart icon" />
                </div>
            </div>
            <div className={style.mobileContainer}>
                <div className={style.logo}>
                    <a href="%PUBLIC_URL%/" ref={logoText}>شا<span>پ</span>ینو</a>
                </div>
                <div className={style.menuIcon} id="menuIconContainer">
                    <svg viewBox="0 0 100 100" className="menu" id='menuIcon' onClick={aniamtionToggle}>
                        <path className="line line1" ref={line1} d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                        <path className="line line2" ref={line2} d="M 20,50 H 80" />
                        <path className="line line3" ref={line3} d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
                    </svg>
                </div>
                <div className={style.menuContainer} ref={menuSlider}>
                    <ul>
                        <li className={style.active}>خانه</li>
                        <li>محصولات</li>
                        <li>ارسال های ما</li>
                        <li>ارتباط با ما</li>
                    </ul>
                    <div className={style.icons}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.399 35.399">
                            <g id="search" transform="translate(-9)">
                                <g id="search-normal" transform="translate(9)">
                                    <path id="Vector" d="M28.024,14.012A14.012,14.012,0,1,1,14.012,0,14.012,14.012,0,0,1,28.024,14.012Z" transform="translate(2.95 2.95)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                    <path id="Vector-2" data-name="Vector" d="M2.95,2.95,0,0" transform="translate(29.499 29.499)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                    <path id="Vector-3" data-name="Vector" d="M0,0H35.4V35.4H0Z" fill="none" opacity="0" />
                                </g>
                            </g>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.399 35.399">
                            <g id="frame">
                                <path id="Vector" d="M0,0A11.8,11.8,0,0,0,8.112,0" transform="translate(13.644 13.349)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Vector-2" data-name="Vector" d="M19.912,0H5.693A5.706,5.706,0,0,0,0,5.693V26.476c0,2.655,1.9,3.776,4.233,2.493l7.2-4a3.125,3.125,0,0,1,2.758,0l7.2,4c2.33,1.3,4.233.177,4.233-2.493V5.693A5.729,5.729,0,0,0,19.912,0Z" transform="translate(4.897 2.95)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Vector-3" data-name="Vector" d="M19.912,0H5.693A5.706,5.706,0,0,0,0,5.693V26.476c0,2.655,1.9,3.776,4.233,2.493l7.2-4a3.125,3.125,0,0,1,2.758,0l7.2,4c2.33,1.3,4.233.177,4.233-2.493V5.693A5.729,5.729,0,0,0,19.912,0Z" transform="translate(4.897 2.95)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Vector-4" data-name="Vector" d="M0,0H35.4V35.4H0Z" fill="none" opacity="0" />
                            </g>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38.958 38.958">
                            <g id="shopping-cart-2" data-name="shopping-cart">
                                <path id="Vector" d="M0,0H2.824A3,3,0,0,1,5.811,3.246L4.464,19.414a4.538,4.538,0,0,0,4.529,4.918H26.28A4.669,4.669,0,0,0,30.841,20.1l.877-12.174a4.5,4.5,0,0,0-4.561-4.886H6.2" transform="translate(3.246 3.246)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Vector-2" data-name="Vector" d="M4.058,2.029A2.029,2.029,0,1,1,2.029,0,2.029,2.029,0,0,1,4.058,2.029Z" transform="translate(24.348 31.653)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Vector-3" data-name="Vector" d="M4.058,2.029A2.029,2.029,0,1,1,2.029,0,2.029,2.029,0,0,1,4.058,2.029Z" transform="translate(11.363 31.653)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Vector-4" data-name="Vector" d="M0,0H19.479" transform="translate(14.609 12.986)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                <path id="Vector-5" data-name="Vector" d="M0,0H38.958V38.958H0Z" fill="none" opacity="0" />
                            </g>
                        </svg>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Navbar;