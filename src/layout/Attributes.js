import React from "react";

//#styling and components importation
import styles from "../scss/components/_Attributes.module.scss";


const Attributes = () => {
  return (
    <>
      <div className={styles.attributesContainer}>
        <div className={styles.attributesTeam}>
          <svg
            id="vuesax_bulk_star"
            data-name="vuesax/bulk/star"
            xmlns="http://www.w3.org/2000/svg"
            width="72.223"
            height="72.223"
            viewBox="0 0 72.223 72.223"
          >
            <g id="star">
              <path
                id="Vector"
                d="M11.376,42.13A5.623,5.623,0,0,0,10.052,37.5L2.74,30.183C.453,27.9-.45,25.458.212,23.352.9,21.245,3.041,19.8,6.23,19.259l9.389-1.565a5.777,5.777,0,0,0,3.641-2.678L24.436,4.634C25.941,1.655,27.987,0,30.214,0s4.273,1.655,5.778,4.634l5.176,10.382a5.428,5.428,0,0,0,2.076,2.046l-32.41,32.41A.613.613,0,0,1,9.811,48.9Z"
                transform="translate(5.897 6.019)"
                fill="#fff"
                opacity="0.4"
              />
              <path
                id="Vector-2"
                data-name="Vector"
                d="M41.077,18.834a5.455,5.455,0,0,0-1.324,4.634l2.076,9.058c.873,3.762.331,6.59-1.535,7.944a4.511,4.511,0,0,1-2.708.813,10.81,10.81,0,0,1-5.326-1.745L23.442,34.3a5.548,5.548,0,0,0-5.056,0L9.569,39.538c-3.34,1.956-6.2,2.287-8.035.933A4.43,4.43,0,0,1,0,38.364L36.593,1.772A5.971,5.971,0,0,1,41.829.086L44.868.6c3.19.542,5.326,1.986,6.019,4.093.662,2.106-.241,4.544-2.528,6.831Z"
                transform="translate(15.197 24.68)"
                fill="#fff"
              />
              <path
                id="Vector-3"
                data-name="Vector"
                d="M0,0H72.223V72.223H0Z"
                fill="none"
                opacity="0"
              />
            </g>
          </svg>
          <span> تضمین اصالت کالا </span>
        </div>

        <div className={styles.attributesTeam}>
          <svg
            id="vuesax_bulk_message"
            data-name="vuesax/bulk/message"
            xmlns="http://www.w3.org/2000/svg"
            width="72.074"
            height="72.074"
            viewBox="0 0 72.074 72.074"
          >
            <g id="message">
              <path
                id="Vector"
                d="M0,32.944V14.985A15,15,0,0,1,15.015,0H45.046A15,15,0,0,1,60.062,14.985V35.947A14.989,14.989,0,0,1,45.046,50.9h-4.5a3.044,3.044,0,0,0-2.4,1.2l-4.5,5.976a4.267,4.267,0,0,1-7.207,0l-4.5-5.976a3.339,3.339,0,0,0-2.4-1.2h-4.5A14.989,14.989,0,0,1,0,35.947Z"
                transform="translate(6.006 6.006)"
                fill="#fff"
                opacity="0.56"
              />
              <path
                id="Vector-2"
                data-name="Vector"
                d="M3,6.006a3,3,0,1,1,3-3A2.993,2.993,0,0,1,3,6.006Z"
                transform="translate(33.034 30.031)"
                fill="#fff"
              />
              <path
                id="Vector-3"
                data-name="Vector"
                d="M3,6.006a3,3,0,1,1,3-3A2.993,2.993,0,0,1,3,6.006Z"
                transform="translate(45.046 30.031)"
                fill="#fff"
              />
              <path
                id="Vector-4"
                data-name="Vector"
                d="M3,6.006a3,3,0,1,1,3-3A2.993,2.993,0,0,1,3,6.006Z"
                transform="translate(21.022 30.031)"
                fill="#fff"
              />
              <path
                id="Vector-5"
                data-name="Vector"
                d="M0,0H72.074V72.074H0Z"
                fill="none"
                opacity="0"
              />
            </g>
          </svg>

          <span> پشتیبانی ۲۴ ساعته </span>
        </div>

        <div className={styles.attributesTeam}>
          <svg
            id="vuesax_bulk_group"
            data-name="vuesax/bulk/group"
            xmlns="http://www.w3.org/2000/svg"
            width="72.148"
            height="72.148"
            viewBox="0 0 72.148 72.148"
          >
            <g id="group">
              <path
                id="Vector"
                d="M0,0H72.148V72.148H0Z"
                fill="none"
                opacity="0"
              />
              <path
                id="Vector-2"
                data-name="Vector"
                d="M60.124,27.056v9.019a9.006,9.006,0,0,1-9.019,9.019H48.1a6.012,6.012,0,1,0-12.025,0H24.049a6.012,6.012,0,1,0-12.025,0H9.019A9.006,9.006,0,0,1,0,36.074V27.056H33.068a6.03,6.03,0,0,0,6.012-6.012V0h5.531a6.062,6.062,0,0,1,5.231,3.036l5.14,8.988H51.105A3.015,3.015,0,0,0,48.1,15.031v9.019a3.015,3.015,0,0,0,3.006,3.006Z"
                transform="translate(6.012 15.031)"
                fill="#fff"
                opacity="0.4"
              />
              <path
                id="Vector-3"
                data-name="Vector"
                d="M12.025,6.012A6.012,6.012,0,1,1,6.012,0,6.012,6.012,0,0,1,12.025,6.012Z"
                transform="translate(18.037 54.111)"
                fill="#fff"
              />
              <path
                id="Vector-4"
                data-name="Vector"
                d="M12.025,6.012A6.012,6.012,0,1,1,6.012,0,6.012,6.012,0,0,1,12.025,6.012Z"
                transform="translate(42.087 54.111)"
                fill="#fff"
              />
              <path
                id="Vector-5"
                data-name="Vector"
                d="M12.025,10.612v4.419H3.006A3.015,3.015,0,0,1,0,12.025V3.006A3.015,3.015,0,0,1,3.006,0H6.884l4.359,7.636A6.028,6.028,0,0,1,12.025,10.612Z"
                transform="translate(54.111 27.056)"
                fill="#fff"
              />
              <g
                id="Group-2"
                data-name="Group"
                transform="translate(0.752 6.012)"
              >
                <path
                  id="Vector-6"
                  data-name="Vector"
                  d="M36.074,0H12.025A12,12,0,0,0,.241,9.77h14.79a2.255,2.255,0,1,1,0,4.509H0v4.509H9.019a2.255,2.255,0,0,1,0,4.509H0v4.509H3.006a2.255,2.255,0,0,1,0,4.509H0v3.758H33.068a6.03,6.03,0,0,0,6.012-6.012V3.006A3.015,3.015,0,0,0,36.074,0Z"
                  transform="translate(5.261)"
                  fill="#fff"
                />
                <path
                  id="Vector-7"
                  data-name="Vector"
                  d="M5.5,0H2.255A2.271,2.271,0,0,0,0,2.255,2.271,2.271,0,0,0,2.255,4.509H5.261V2.255A11.024,11.024,0,0,1,5.5,0Z"
                  transform="translate(0 9.77)"
                  fill="#fff"
                />
                <path
                  id="Vector-8"
                  data-name="Vector"
                  d="M2.255,0A2.271,2.271,0,0,0,0,2.255,2.271,2.271,0,0,0,2.255,4.509H5.261V0Z"
                  transform="translate(0 18.789)"
                  fill="#fff"
                />
                <path
                  id="Vector-9"
                  data-name="Vector"
                  d="M2.255,0A2.271,2.271,0,0,0,0,2.255,2.271,2.271,0,0,0,2.255,4.509H5.261V0Z"
                  transform="translate(0 27.807)"
                  fill="#fff"
                />
              </g>
            </g>
          </svg>
          <span> ارسال سریع کالا </span>
        </div>

        <div className={styles.attributesTeam}>
          <svg
            id="vuesax_bulk_shield-tick"
            data-name="vuesax/bulk/shield-tick"
            xmlns="http://www.w3.org/2000/svg"
            width="72.148"
            height="72.148"
            viewBox="0 0 72.148 72.148"
          >
            <g id="shield-tick">
              <path
                id="Vector"
                d="M22.276.47,5.742,6.663A9.592,9.592,0,0,0,0,14.99V39.34a9.981,9.981,0,0,0,3.547,7.095L20.081,58.79a9.375,9.375,0,0,0,10.612,0L47.227,46.434a10.038,10.038,0,0,0,3.547-7.095V14.99a9.548,9.548,0,0,0-5.742-8.3L28.5.5A10.227,10.227,0,0,0,22.276.47Z"
                transform="translate(10.672 5.723)"
                fill="#fff"
                opacity="0.56"
              />
              <path
                id="Vector-2"
                data-name="Vector"
                d="M7.087,17.428a2.23,2.23,0,0,1-1.593-.661l-4.84-4.84A2.253,2.253,0,0,1,3.84,8.74l3.247,3.247L18.42.654A2.253,2.253,0,0,1,21.607,3.84L8.68,16.767A2.23,2.23,0,0,1,7.087,17.428Z"
                transform="translate(24.959 25.35)"
                fill="#fff"
              />
              <path
                id="Vector-3"
                data-name="Vector"
                d="M0,0H72.148V72.148H0Z"
                fill="none"
                opacity="0"
              />
            </g>
          </svg>
          <span> پرداخت آنلاین و ایمن </span>
        </div>
      </div>
    </>
  );
};

export default Attributes;
