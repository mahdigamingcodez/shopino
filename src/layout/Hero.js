//#root importations
import React, { useState } from 'react';

//#styling and components importation
import style from '../scss/layout/_Hero.module.scss';

//#image importation
import blackHoody from '../images/black-hoody.png';
import tshirt from '../images/t-shirt.png';

const Hero = () => {

    const changeImageSrc = (anything) => {
        document.querySelector('#mainItem').src = anything;
    };

    const [size, setSize] = useState({
        activeSize: null,
        sizes: [
            { size: "SM", id: 1 },
            { size: "M", id: 2 },
            { size: "L", id: 3 },
            { size: "XL", id: 4 },
            { size: "XXL", id: 5 },
            { size: "XXL", id: 6 }
        ]
    });

    const toggleActive = (index) => {
        setSize({ ...size, activeSize: size.sizes[index] })
    };

    const toggleActiveStyles = (index) => {
        if (size.sizes[index] === size.activeSize) {
            return `${style.items} , ${style.active}`;
        } else {
            return `${style.items} , ${style.inActive}`;
        }
    };

    const [colors, setColors] = useState({
        activeColor: null,
        colors: [
            { id: 1 },
            { id: 2 },
            { id: 3 },
            { id: 4 },
        ]
    });

    const toggleActiveColors = (index) => {
        setColors({ ...colors, activeColor: colors.colors[index] })
        // console.log(colors.colors[index])
    };

    const toggleActiveColorsStyle = (index) => {
        if (colors.colors[index] === colors.activeColor) {
            return `${style.colors} , ${style.activeColor}`;
        } else {
            return `${style.colors} , ${style.unActiveColor}`;
        }
    };

    return (
        // Wrapper All The Sections
        <section className={style.wrapper}>
            {/* Header Section */}
            <header>
                <h1>بهترین ها رو برای <mark>خودت</mark> انتخاب کن !!!</h1>
                <p>
                    ما این کار را برای تــــو راحت میکنیم.
                    فقط کافیه یه چرخی تو وبسایت ما بزنی،
                    خاص ترین محصولات رو برای تو فراهم کردیم
                </p>
                <nav>
                    <a href="https://somewere.com">
                        ویتــــــرین
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="vuesax_linear_receive-square" data-name="vuesax/linear/receive-square" transform="translate(-748 -380)">
                                <g id="receive-square">
                                    <path id="Vector" d="M7,20h6c5,0,7-2,7-7V7c0-5-2-7-7-7H7C2,0,0,2,0,7v6C0,18,2,20,7,20Z" transform="translate(750 382.5)" fill="none" stroke="#fca12b" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                    <g id="Group" transform="translate(0 1.49)">
                                        <g id="Group-2" data-name="Group">
                                            <path id="Vector-2" data-name="Vector" d="M0,0,3,3,6,0" transform="translate(757 391.51)" fill="none" stroke="#1a1c20" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                            <path id="Vector-3" data-name="Vector" d="M0,8V0" transform="translate(760 386.51)" fill="none" stroke="#1a1c20" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                        </g>
                                    </g>
                                    <path id="Vector-4" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(748 380)" fill="none" opacity="0" />
                                </g>
                            </g>
                        </svg>
                    </a>
                </nav>
            </header>
            {/* Section Pagination Slider*/}
            <section className={style.paginationSection}>
                <div className={style.pagination}>
                    <img src={blackHoody} onClick={() => changeImageSrc(`${blackHoody}`)} alt="black hoody" />
                </div>
                <div className={style.pagination}>
                    <img src={tshirt} onClick={() => changeImageSrc(`${tshirt}`)} alt="tshirt cloth" />
                </div>
                <div className={style.pagination}>
                    <img src={blackHoody} onClick={() => changeImageSrc(`${blackHoody}`)} alt="black hoody" />
                </div>
                <div className={style.pagination}>
                    <img src={tshirt} onClick={() => changeImageSrc(`${tshirt}`)} alt="tshirt cloth" />
                </div>
            </section>
            {/* Section Slider*/}
            <section className={style.slider}>
                <img src={blackHoody} className={style.mainItem} id="mainItem" alt="black hoody" />
            </section>
            {/* Section Product Control Panel*/}
            <section className={style.productControlPanel}>
                <div className={style.colorsPanel}>
                    <h4>رنگ :</h4>
                    <div className={style.colorContainer}>
                        {colors.colors.map((element, index) => (
                            <div key={index} className={toggleActiveColorsStyle(index)} onClick={() => toggleActiveColors(index)}></div>
                        ))}
                    </div>
                </div>
                <div className={style.sizePanel}>
                    <h4>سایز :</h4>
                    <div className={style.sizesContainer}>
                        {size.sizes.map((element, index) => (
                            <div key={index} className={toggleActiveStyles(index)} onClick={() => toggleActive(index)}>{element.size}</div>
                        ))}
                    </div>
                </div>
                <div className={style.buyAndSave}>
                    <button>
                        خریدن
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="vuesax_linear_bag-happy" data-name="vuesax/linear/bag-happy" transform="translate(-172 -188)">
                                <g id="bag-happy">
                                    <path id="Vector" d="M0,0A3.517,3.517,0,0,0,3.5,3.5,3.517,3.517,0,0,0,7,0" transform="translate(180.5 202.25)" fill="none" stroke="#F9B356" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                    <path id="Vector-2" data-name="Vector" d="M3.62,0,0,3.63" transform="translate(177.19 190)" fill="none" stroke="#F9B356" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                    <path id="Vector-3" data-name="Vector" d="M0,0,3.62,3.63" transform="translate(187.19 190)" fill="none" stroke="#F9B356" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                    <path id="Vector-4" data-name="Vector" d="M0,2C0,.15.99,0,2.22,0H17.78C19.01,0,20,.15,20,2c0,2.15-.99,2-2.22,2H2.22C.99,4,0,4.15,0,2Z" transform="translate(174 193.85)" fill="none" stroke="#F9B356" strokeWidth="1.5" />
                                    <path id="Vector-5" data-name="Vector" d="M0,0,1.41,8.64C1.73,10.58,2.5,12,5.36,12h6.03c3.11,0,3.57-1.36,3.93-3.24L17,0" transform="translate(175.5 198)" fill="none" stroke="#F9B356" strokeLinecap="round" strokeWidth="1.5" />
                                    <path id="Vector-6" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(172 188)" fill="none" opacity="0" />
                                </g>
                            </g>
                        </svg>
                    </button>
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 35.399 35.399">
                        <g id="frame">
                            <path id="Vector" d="M0,0A11.8,11.8,0,0,0,8.112,0" transform="translate(13.644 13.349)" fill="none" stroke="#FFA733" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" />
                            <path id="Vector-2" data-name="Vector" d="M19.912,0H5.693A5.706,5.706,0,0,0,0,5.693V26.476c0,2.655,1.9,3.776,4.233,2.493l7.2-4a3.125,3.125,0,0,1,2.758,0l7.2,4c2.33,1.3,4.233.177,4.233-2.493V5.693A5.729,5.729,0,0,0,19.912,0Z" transform="translate(4.897 2.95)" fill="none" stroke="#FFA733" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" />
                            <path id="Vector-3" data-name="Vector" d="M19.912,0H5.693A5.706,5.706,0,0,0,0,5.693V26.476c0,2.655,1.9,3.776,4.233,2.493l7.2-4a3.125,3.125,0,0,1,2.758,0l7.2,4c2.33,1.3,4.233.177,4.233-2.493V5.693A5.729,5.729,0,0,0,19.912,0Z" transform="translate(4.897 2.95)" fill="none" stroke="#FFA733" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" />
                            <path id="Vector-4" data-name="Vector" d="M0,0H35.4V35.4H0Z" fill="none" opacity="0" />
                        </g>
                    </svg>
                </div>
            </section>
            {/* Section Slider Mini-laptop & Tablet */}
            <section className={style.sliderAndPagination}>
                <section className={style.slider}>
                    <img src={blackHoody} className={style.mainItem} id="mainItem" alt="black hoody" />
                </section>
                <section className={style.paginationSection}>
                    <div className={style.pagination}>
                        <img src={blackHoody} onClick={() => changeImageSrc(`${blackHoody}`)} alt="black hoody" />
                    </div>
                    <div className={style.pagination}>
                        <img src={tshirt} onClick={() => changeImageSrc(`${tshirt}`)} alt="tshirt cloth" />
                    </div>
                    <div className={style.pagination}>
                        <img src={blackHoody} onClick={() => changeImageSrc(`${blackHoody}`)} alt="black hoody" />
                    </div>
                    <div className={style.pagination}>
                        <img src={tshirt} onClick={() => changeImageSrc(`${tshirt}`)} alt="tshirt cloth" />
                    </div>
                </section>
            </section>
        </section>
    );
};

export default Hero;