//#root Importation
import React from 'react';

//style and Components importation
import '../_App.scss';
import style from '../scss/layout/_CheckCart.module.scss';
import '../_Colors.scss';
import Navbar from './Navbar';
import ShopingProgress from '../components/ShopingProgress';
import ProductBar from '../components/ProductBar';
import Proceed from '../components/Proceed';
// import Card from '../components/Card';
import Attributes from '../layout/Attributes';
import Footer from '../layout/Footer';

//images importation
import tshirt from '../images/t-shirt.png';
import blackHoody from '../images/black-hoody.png';
import checkSign from '../images/check-sign.png';

const CheckCart = () => {

    return (
        <div className={style.wrapper}>

            <Navbar navLink={`hide`} />

            <section className={style.shoppingProgressContainer}>
                <ShopingProgress number="۱" sign={checkSign} />
            </section>

            <section className={style.cartDetailsContainer}>
                <section className={style.productsContainer}>
                    <ProductBar imageSrc={tshirt} imageAlt="t-shirt cloth" productName="تی شرت  A _ 17" color={`colors grey`} size="M" price="۳۰۰.۰۰۰" />
                    <ProductBar imageSrc={blackHoody} imageAlt="t-shirt cloth" productName="هودی آدیداس A _ 17" color={`colors black`} size="L" price="۳۰۰.۰۰۰" />
                </section>
                <section className={style.proceedContainer}>
                    <Proceed amount="۲" price="۶۰۰.۰۰۰" navTitle="ادامه فرایند خرید" />
                </section>
            </section>

            {/* <section className={style.randomProducts}>
                <h2 className={style.title}>
                    محصولات دیگر
                </h2>
                <div className={style.cardsContainer}>
                    <Card imageSrc={blackHoody} imageAlt="هودی" productName="هودی" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={blackHoody} imageAlt="هودی" productName="هودی" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                    <Card imageSrc={tshirt} imageAlt="تی شرت" productName="تی شرت" smallInfo="گرم و نخی" price="۳۰۰/۰۰۰ ت" />
                </div>
            </section> */}


            <div className={style.attributesContainer}>
                <Attributes />
            </div>

            <div className={style.footerContainer}>
                <Footer />
            </div>

        </div>
    );
};

export default CheckCart;