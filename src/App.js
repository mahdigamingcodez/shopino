//#root importations
import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import CartDetails from './pages/CartDetails';
import ProductDetails from './components/ProductDetails';

//#styling and components importation
import './_App.scss';

const App = () => {
  return (
    <>
      <section className='unsupported-container'>
        <h2>دستگاه شما پشتیبانی نمی شود.</h2>
      </section>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/cart-details" element={< CartDetails />} />
        <Route path="/product-details" element={< ProductDetails />} />
      </Routes>
    </>
  )
}
export default App;
