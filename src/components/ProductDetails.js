import React , { useState } from 'react';

//styling
import style from "../scss/components/_ProductDetails.module.scss";

//Images & Icons
import shirt from "../images/shirt.png";
import namad from "../images/namad1.svg";

const ProductDetails = () => {
    const changeImageSrc = (anything) => {
        document.querySelector('#paginationImage').src = anything;
    }

    const [colors , setColors] = useState ({
        activeColor: null,
        colors: [
            { id: 1 },
            { id: 2 },
            { id: 3 }
        ]
    });

    const toggleActiveColors = (index) => {
        setColors({ ...colors, activeColor: colors.colors[index] })
    };

    const toggleActiveColorsStyle = (index) => {
        if (colors.colors[index] === colors.activeColor) {
            return `${style.colors} , ${style.activeColor}`;
        } else {
            return `${style.colors} , ${style.unActiveColor}`;
        }
    };

    const [size, setSize] = useState ({
        activeSiz: null,
        sizes: [
            { size: "SM", id:1 },
            { size: "M", id:2 },
            { size: "L", id:3 },
            { size: "XL", id:4 },
            { size: "XXL", id:5 }
        ]
    });

    const toggleActive = (index) => {
        setSize({ ...size, activeSize: size.sizes[index] })
    };

    const toggleActiveStyles = (index) => {
        if (size.sizes[index] === size.activeSize) {
            return `${style.items} , ${style.active}`;
        } else {
            return `${style.items} , ${style.inActive}`;
        }
    };

    return (
        <>
            <section className={style.main}>
                <header className={style.productsText}>
                    <h1> هودی آدیداس مدل A-17 </h1>
                    <h4> گرم و نخی </h4>
                    <p>
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از 
                    </p>
                    
                    <h3> دسته بندی: <span> تی شرت </span> </h3>
                    <h3> قیمت نهایی: <span> ۳۰۰.۰۰۰ <i> تومان </i> </span> </h3>
                    <div className={style.productColor}>
                        <h3> رنگ: </h3>
                        <div className={style.colorContainer}>
                            {colors.colors.map((element, index) => (
                                <div key={index} className={toggleActiveColorsStyle(index)} onClick={() => toggleActiveColors(index)}> </div>
                            ))}
                        </div>
                    </div>

                    <div className={style.productSize}>
                        <h3> سایز: </h3>
                        <div className={style.sizesContainer}>
                            {size.sizes.map((element, index) => (
                                <div key={index} className={toggleActiveStyles(index)} onClick={() => toggleActive(index)}>{element.size}</div>
                            ))}
                        </div>
                    </div>

                    <button> افزودن به سبد خرید </button>
                </header>

                <section className={style.productImage}>
                    <div className={style.productBGImage}>
                        <img src={shirt} alt="productImage"
                        id='paginationImage' />
                    </div>
                </section>

                <section className={style.pagination}>
                    <div className={style.paginationBGImage}>
                        <img src={shirt} alt="paginationImage" 
                        onClick={() => changeImageSrc(`${shirt}`)}  />
                    </div>

                    <div className={style.paginationBGImage}>
                        <img src={namad} alt="paginationImage" 
                        onClick={() => changeImageSrc(`${namad}`)} />
                    </div>

                    <div className={style.paginationBGImage}>
                        <img src={namad} alt="paginationImage"
                        onClick={() => changeImageSrc(`${namad}`)} />
                    </div>

                    <div className={style.paginationBGImage}>
                        <img src={shirt} alt="paginationImage" 
                        onClick={() => changeImageSrc(`${shirt}`)}/>
                    </div>
                </section>
            </section>
        </>
    );
};

export default ProductDetails;