//#root importation
import React, { useState } from 'react';

//#packages importation
import parse from 'html-react-parser';

//#styling and components importation
import style from '../scss/components/_Filters.module.scss';

const Filters = () => {

    const [filter, setFilter] = useState({
        activeFilters: null,
        filters: [
            {
                type: ` جدید ترین ها
                <svg id="vuesax_linear_add-square" data-name="vuesax/linear/add-square" xmlns="http://www.w3.org/2000/svg" width="29.514" height="29.514" viewBox="0 0 29.514 29.514">
  <g id="add-square" transform="translate(0)">
    <path id="Vector" d="M9.838,0H0" transform="translate(9.838 14.757)" fill="none" stroke="#1a1c20" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-2" data-name="Vector" d="M0,9.838V0" transform="translate(14.757 9.838)" fill="none" stroke="#1a1c20" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-3" data-name="Vector" d="M15.987,24.6H8.608C2.46,24.6,0,22.136,0,15.987V8.608C0,2.46,2.46,0,8.608,0h7.379C22.136,0,24.6,2.46,24.6,8.608v7.379C24.6,22.136,22.136,24.6,15.987,24.6Z" transform="translate(2.46 2.46)" fill="none" stroke="#1a1c20" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-4" data-name="Vector" d="M29.514,0H0V29.514H29.514Z" fill="none" opacity="0"/>
  </g>
</svg>


`, id: 1,
            },
            {
                type: `تخفیف دار ها
      <svg id="vuesax_linear_ticket-discount" data-name="vuesax/linear/ticket-discount" xmlns="http://www.w3.org/2000/svg" width="29.514" height="29.514" viewBox="0 0 29.514 29.514">
  <g id="ticket-discount">
    <path id="Vector" d="M21.521,10.453A3.076,3.076,0,0,1,24.6,7.379V6.149C24.6,1.23,23.365,0,18.446,0H6.149C1.23,0,0,1.23,0,6.149v.615A3.075,3.075,0,0,1,3.074,9.838,3.075,3.075,0,0,1,0,12.912v.615c0,4.919,1.23,6.149,6.149,6.149h12.3c4.919,0,6.149-1.23,6.149-6.149A3.076,3.076,0,0,1,21.521,10.453Z" transform="translate(2.46 4.919)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-2" data-name="Vector" d="M0,7.379,7.379,0" transform="translate(11.068 10.76)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-3" data-name="Vector" d="M.495.5H.506" transform="translate(17.945 17.639)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-4" data-name="Vector" d="M.495.5H.506" transform="translate(10.567 10.875)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-5" data-name="Vector" d="M0,0H29.514V29.514H0Z" fill="none" opacity="0"/>
  </g>
</svg>

`, id: 2,
            },
            {
                type: ` پرفروش ترین ها
      <svg id="vuesax_linear_ranking" data-name="vuesax/linear/ranking" xmlns="http://www.w3.org/2000/svg" width="29.514" height="29.514" viewBox="0 0 29.514 29.514">
  <g id="ranking" transform="translate(0 0)">
    <path id="Vector" d="M8.2,0H2.46A2.467,2.467,0,0,0,0,2.46V9.838H8.2Z" transform="translate(2.46 17.217)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-2" data-name="Vector" d="M5.743,0H2.46A2.467,2.467,0,0,0,0,2.46v12.3H8.2V2.46A2.459,2.459,0,0,0,5.743,0Z" transform="translate(10.65 12.298)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path id="Vector-3" data-name="Vector" d="M5.743,0H0V6.149H8.2V2.46A2.467,2.467,0,0,0,5.743,0Z" transform="translate(18.852 20.906)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <g id="Group" transform="translate(11.06 2.021)">
      <path id="Vector-4" data-name="Vector" d="M4.336.524l.652,1.3a.743.743,0,0,0,.516.381l1.181.2c.75.123.935.676.394,1.205l-.922.922a.786.786,0,0,0-.2.664l.258,1.131c.209.9-.271,1.242-1.058.775L4.053,6.452a.76.76,0,0,0-.726,0L2.221,7.1c-.787.467-1.267.123-1.058-.775L1.422,5.2a.775.775,0,0,0-.2-.664l-.91-.91c-.541-.541-.369-1.082.394-1.205l1.181-.2a.82.82,0,0,0,.516-.381l.652-1.3C3.414-.177,3.98-.177,4.336.524Z" transform="translate(0)" fill="none" stroke="#a2a2a2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    </g>
    <path id="Vector-5" data-name="Vector" d="M0,0H29.514V29.514H0Z" fill="none" opacity="0"/>
  </g>
</svg>

`, id: 3,
            },
        ]
    });

    const toggleFilter = (index) => {
        setFilter({ ...filter, activeFilters: filter.filters[index] })
    };

    const toggleFilterActive = (index) => {
        if (filter.filters[index] === filter.activeFilters) {
            return `${style.filters} , ${style.activeFilter} `;
        } else {
            return `${style.filters} , ${style.unActiveFilter} `;
        }
    };

    return (
        <div className={style.container}>
            {filter.filters.map((element, index) => (
                <div key={index} className={toggleFilterActive(index)} onClick={() => toggleFilter(index)}>
                    {parse(element.type)}
                </div>
            ))}
        </div >
    );
};

export default Filters;