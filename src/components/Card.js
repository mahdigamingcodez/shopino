//#root importations
import React from 'react';

//#styling and components importation
import style from '../scss/components/_Card.module.scss';

const Card = (props) => {
    const { imageSrc, imageAlt, productName, smallInfo, price } = props;
    return (
        <div className={style.cardContainer}>
            <div className={style.imageContainer}>
                <img src={imageSrc} alt={imageAlt} />
            </div>
            <div className={style.productSummery}>
                <h2>{productName}</h2>
                <h3>{smallInfo}</h3>
                <svg xmlns="http://www.w3.org/2000/svg" className={style.saveIcon} viewBox="0 0 24 24">
                    <g id="vuesax_linear_frame" data-name="vuesax/linear/frame" transform="translate(-172 -188)">
                        <g id="frame">
                            <path id="Vector" d="M0,0A8,8,0,0,0,5.5,0" transform="translate(181.25 197.05)" fill="none" stroke="#989898" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                            <path id="Vector-2" data-name="Vector" d="M13.5,0H3.86A3.869,3.869,0,0,0,0,3.86V17.95c0,1.8,1.29,2.56,2.87,1.69l4.88-2.71a2.118,2.118,0,0,1,1.87,0l4.88,2.71c1.58.88,2.87.12,2.87-1.69V3.86A3.884,3.884,0,0,0,13.5,0Z" transform="translate(175.32 190)" fill="none" stroke="#989898" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                            <path id="Vector-3" data-name="Vector" d="M13.5,0H3.86A3.869,3.869,0,0,0,0,3.86V17.95c0,1.8,1.29,2.56,2.87,1.69l4.88-2.71a2.118,2.118,0,0,1,1.87,0l4.88,2.71c1.58.88,2.87.12,2.87-1.69V3.86A3.884,3.884,0,0,0,13.5,0Z" transform="translate(175.32 190)" fill="none" stroke="#989898" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                            <path id="Vector-4" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(172 188)" fill="none" opacity="0" />
                        </g>
                    </g>
                </svg>
                <div className={style.priceAndPeek}>
                    <button>مشاهده <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g id="vuesax_linear_eye" data-name="vuesax/linear/eye" transform="translate(-108 -188)">
                            <g id="eye">
                                <path id="Vector" d="M7.16,3.58A3.58,3.58,0,1,1,3.58,0,3.576,3.576,0,0,1,7.16,3.58Z" transform="translate(116.42 196.42)" fill="none" stroke="#d4d4d4" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                <path id="Vector-2" data-name="Vector" d="M9.785,16.55c3.53,0,6.82-2.08,9.11-5.68a5.326,5.326,0,0,0,0-5.19C16.6,2.08,13.315,0,9.785,0S2.965,2.08.675,5.68a5.326,5.326,0,0,0,0,5.19C2.965,14.47,6.255,16.55,9.785,16.55Z" transform="translate(110.215 191.72)" fill="none" stroke="#d4d4d4" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" />
                                <path id="Vector-3" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(132 212) rotate(180)" fill="none" opacity="0" />
                            </g>
                        </g>
                    </svg>
                    </button>
                    <span>{price}</span>
                </div>
            </div>
        </div>
    );
};

export default Card;