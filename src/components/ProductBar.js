//#root Importation
import React, { useState } from 'react';

//style and Components importation
import style from '../scss/components/_ProductBar.module.scss';

const ProductBar = (props) => {

    const [productAmount, setProductAmount] = useState({
        amount: 1,
    })

    const { imageSrc, imageAlt, productName, color, size, price } = props;

    return (
        <section className={style.wrapper}>
            <div className={style.productImage}>
                <img src={imageSrc} alt={imageAlt} />
            </div>
            <div className={style.productInformation}>
                <header className={style.primeInfo}>
                    <h2>{productName}</h2>
                    <div className={style.selectedColor}>
                        رنگ :
                        <div className={color}>

                        </div>
                    </div>
                    <div className={style.selectedSize}>
                        سایز :
                        <div>
                            {size}
                        </div>
                    </div>
                </header >
                <section className={style.attributes}>
                    <p>
                        <svg id="vuesax_linear_shield-tick" data-name="vuesax/linear/shield-tick" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="shield-tick">
                                <path id="Vector" d="M7.08.233,2.09,2.112A3.47,3.47,0,0,0,0,5.122v7.43a4.862,4.862,0,0,0,1.73,3.44l4.3,3.21a4.552,4.552,0,0,0,5.14,0l4.3-3.21a4.862,4.862,0,0,0,1.73-3.44V5.122A3.472,3.472,0,0,0,15.11,2.1L10.12.233A5.085,5.085,0,0,0,7.08.233Z" transform="translate(3.41 1.997)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                <path id="Vector-2" data-name="Vector" d="M0,2.69,1.61,4.3,5.91,0" transform="translate(9.05 9.18)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                <path id="Vector-3" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(24 24) rotate(180)" fill="none" opacity="0" />
                            </g>
                        </svg>
                        گارانتی اصالت و سلامت فیزیکی کالا
                    </p>
                    <p>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="vuesax_linear_shop" data-name="vuesax/linear/shop" transform="translate(0 2)">
                                <g id="shop" transform="translate(0 -2)">
                                    <path id="Vector" d="M0,0V4.49c0,4.49,1.8,6.29,6.29,6.29h5.39c4.49,0,6.29-1.8,6.29-6.29V0" transform="translate(3.01 11.22)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-2" data-name="Vector" d="M3.016,10a2.965,2.965,0,0,0,3-3.32L5.356,0H.686L.016,6.68A2.965,2.965,0,0,0,3.016,10Z" transform="translate(8.984 2)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-3" data-name="Vector" d="M4.01,10a3.257,3.257,0,0,0,3.3-3.65L7.03,3.6C6.67,1,5.67,0,3.05,0H0L.7,7.01A3.408,3.408,0,0,0,4.01,10Z" transform="translate(14.3 2)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-4" data-name="Vector" d="M3.318,10a3.39,3.39,0,0,0,3.3-2.99l.22-2.21L7.318,0H4.268C1.648,0,.648,1,.288,3.6L.018,6.35A3.257,3.257,0,0,0,3.318,10Z" transform="translate(2.322 2)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-5" data-name="Vector" d="M2.5,0A2.207,2.207,0,0,0,0,2.5V5H5V2.5A2.207,2.207,0,0,0,2.5,0Z" transform="translate(9.5 17)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-6" data-name="Vector" d="M0,0H24V24H0Z" fill="none" opacity="0" />
                                </g>
                            </g>
                        </svg>
                        فروشگاه شا‍‍پینو
                    </p>
                    <p>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="vuesax_linear_group" data-name="vuesax/linear/group" transform="translate(-684 -188)">
                                <g id="group">
                                    <path id="Vector" d="M13,0V10a2.006,2.006,0,0,1-2,2H0V4A4,4,0,0,1,4,0Z" transform="translate(686 190)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-2" data-name="Vector" d="M20,9v3a3,3,0,0,1-3,3H16a2,2,0,0,0-4,0H8a2,2,0,0,0-4,0H3a3,3,0,0,1-3-3V9H11a2.006,2.006,0,0,0,2-2V0h1.84a2.016,2.016,0,0,1,1.74,1.01L18.29,4H17a1,1,0,0,0-1,1V8a1,1,0,0,0,1,1Z" transform="translate(686 193)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-3" data-name="Vector" d="M4,2A2,2,0,1,1,2,0,2,2,0,0,1,4,2Z" transform="translate(690 206)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-4" data-name="Vector" d="M4,2A2,2,0,1,1,2,0,2,2,0,0,1,4,2Z" transform="translate(698 206)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-5" data-name="Vector" d="M4,3V5H1A1,1,0,0,1,0,4V1A1,1,0,0,1,1,0H2.29Z" transform="translate(702 197)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-6" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(684 188)" fill="none" opacity="0" />
                                </g>
                            </g>
                        </svg>
                        ارسال به سر تا سر ایران
                    </p>
                </section>
                <section className={style.amountContainer}>
                    <div className={style.row}>
                        <div className={style.increaseAndDecrease}>
                            <div className={style.increase}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <g id="vuesax_linear_add" data-name="vuesax/linear/add" transform="translate(-492 -252)">
                                        <g id="add">
                                            <path id="Vector" d="M0,0H12" transform="translate(498 264)" fill="none" stroke="#b4b4b4" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                            <path id="Vector-2" data-name="Vector" d="M0,12V0" transform="translate(504 258)" fill="none" stroke="#b4b4b4" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                            <path id="Vector-3" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(492 252)" fill="none" opacity="0" />
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <span>{productAmount.amount}</span>
                            <div className={style.decrease}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <g id="vuesax_linear_minus" data-name="vuesax/linear/minus" transform="translate(-556 -252)">
                                        <g id="minus">
                                            <path id="Vector" d="M0,0H12" transform="translate(562 264)" fill="none" stroke="#b4b4b4" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                            <path id="Vector-2" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(556 252)" fill="none" opacity="0" />
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div className={style.deleteContainer}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g id="vuesax_linear_trash" data-name="vuesax/linear/trash" transform="translate(-108 -188)">
                                <g id="trash">
                                    <path id="Vector" d="M18,.5C14.67.17,11.32,0,7.98,0A59.068,59.068,0,0,0,2.04.3L0,.5" transform="translate(111 193.48)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-2" data-name="Vector" d="M0,2.97.22,1.66C.38.71.5,0,2.19,0H4.81C6.5,0,6.63.75,6.78,1.67L7,2.97" transform="translate(116.5 190)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-3" data-name="Vector" d="M13.7,0l-.65,10.07c-.11,1.57-.2,2.79-2.99,2.79H3.64C.85,12.86.76,11.64.65,10.07L0,0" transform="translate(113.15 197.14)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-4" data-name="Vector" d="M0,0H3.33" transform="translate(118.33 204.5)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-5" data-name="Vector" d="M0,0H5" transform="translate(117.5 200.5)" fill="none" stroke="#989898" strokeLinecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Vector-6" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(108 188)" fill="none" opacity="0" />
                                </g>
                            </g>
                        </svg>
                        <span>
                            حذف
                        </span>
                    </div>
                    <div className={style.priceContainer}>
                        <span>{price} <i>تومان</i></span>
                    </div>
                </section>
            </div>
        </section >
    );
};

export default ProductBar;