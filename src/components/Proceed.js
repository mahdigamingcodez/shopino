//#root Importation
import React from 'react';
import { Link } from 'react-router-dom';

//style and Components importation
import style from '../scss/components/_Proceed.module.scss';

const Proceed = (props) => {

    const { amount, price, navTitle } = props

    return (
        <section className={style.container}>
            <header>
                <div className={style.row}>
                    <h2>
                        تعداد کالا ها
                    </h2>
                    <span>{amount} <i>عدد</i></span>
                </div>
                <div className={style.row}>
                    <h3>
                        جمع سبد خرید
                    </h3>
                    <span>{price} <i>تومان</i></span>
                </div>
            </header>
            <footer>
                <p>
                    هزینه ارسال در ادامه بر اساس آدرس،زمان و نحوه ی ارسال انتخابی شما
                    محاسبه و به این مبلغ اضافه خواهد شد.
                </p>
            </footer>
            <nav>
                <Link to='/'>{navTitle}</Link>
            </nav>
        </section>
    );
};

export default Proceed;