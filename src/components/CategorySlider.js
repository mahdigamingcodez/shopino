//#root importations
import React, { useState } from "react";

//#import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

//#import Swiper styles
import "swiper/scss";
import "swiper/scss/navigation";

//#import required modules
import { Navigation } from "swiper";

//#styling and components importation
import style from "../scss/components/_CategorySlider.module.scss";

//Images and Icons
import tshirt from "../images/t-shirt.png";
import hodi from "../images/black-hoody.png";
import pants from "../images/pants.png";
import shoes from "../images/shoes.png";
import hat from "../images/hat.png";

const CategorySlider = () => {
  const [nav, setNav] = useState({
    activeNav: null,
    navs: [
      { nav: " تی شرت", id: 1, link: "#item1" },
      { nav: "هــــودی", id: 2, link: "#item2" },
      { nav: "شلــــــوار", id: 3, link: "#item3" },
      { nav: "کفش و کتونی", id: 4, link: "#item4" },
      { nav: "کـــلاه", id: 5, link: "#item5" },
    ],
  });

  const toggleActive = (index) => {
    setNav({ ...nav, activeNav: nav.navs[index] });
  };

  const toggleActiveStyles = (index) => {
    if (nav.navs[index] === nav.activeNav) {
      return `${style.items} , ${style.active}`;
    } else {
      return `${style.items} , ${style.inActive}`;
    }
  };

  return (
    <>
      <div className={style.csContainer}>
        <div className={style.Title}>
          <h2> دسته بندی ها </h2>
        </div>

        <Swiper
          navigation={true}
          modules={[Navigation]}
          className={style.slider}
        >
          <SwiperSlide className={style.swiperSlide}>
            <div className={style.sliderItemContainer}>
              <div className={style.imagesContainer}>
                <div className={style.imageBg}>
                  <img src={tshirt} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={hodi} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={pants} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={shoes} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={hat} alt="sliderimagecategory" />
                </div>
              </div>

              <div className={style.navContainer}>
                {nav.navs.map((element, index) => (
                  <a
                    href={element.link}
                    key={index}
                    className={toggleActiveStyles(index)}
                    onClick={() => toggleActive(index)}
                  >
                    {element.nav}
                  </a>
                ))}
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide className={style.swiperSlide}>
            <div className={style.sliderItemContainer}>
              <div className={style.imagesContainer}>
                <div className={style.imageBg}>
                  <img src={tshirt} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={hodi} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={pants} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={shoes} alt="sliderimagecategory" />
                </div>
                <div className={style.imageBg}>
                  <img src={hat} alt="sliderimagecategory" />
                </div>
              </div>

              <div className={style.navContainer}>
                {nav.navs.map((element, index) => (
                  <a
                    href={element.link}
                    key={index}
                    className={toggleActiveStyles(index)}
                    onClick={() => toggleActive(index)}
                  >
                    {element.nav}
                  </a>
                ))}
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </div>
    </>
  );
};

export default CategorySlider;
