//#root importations
import React from 'react';

//#styling and components importation
import style from '../scss/components/_Vitrin.module.scss';
import '../_App.scss';


const Vitrin = (props) => {

    const { vitrinTitle, vtImageSrc1, vtImageSrc2, vtImageSrc3, vtImageSrc4, vtImageSrc5, vtImageSrc6,
        vtImageSrc7, vtImageSrc8, vtImageSrc9, vitrinImageAlt } = props

    return (
        <section className={style.vitrinWrapper}>
            <header>
                <h2>{vitrinTitle}</h2>
            </header>
            <section className={style.vitrinContainer}>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc1} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc2} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc3} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc4} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc5} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc6} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc7} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc8} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
                <div className={style.vitrinItems}>
                    <img src={vtImageSrc9} alt={vitrinImageAlt} />
                    {/* <input type={"file"} className={panelOrHome} /> */}
                </div>
            </section>
        </section>
    );
};

export default Vitrin;