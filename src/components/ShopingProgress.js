//#root importations
import React from 'react';

//#styling and components importation
import style from '../scss/components/_ShoppingProgress.module.scss';

const ShopingProgress = (props) => {

    const { sign, number } = props;

    return (
        <div className={style.wrapper}>
            <div className={style.container}>
                <div className={style.numbersIndicatorContainer}>
                    <div className={style.current}>
                        <div className={style.number}>
                            {number}
                            {/* <img src={sign} alt="check sign" /> */}
                        </div>
                        <div className={style.text}>
                            سبد خرید
                        </div>
                    </div>
                    <div className={style.disable}>
                        <div className={style.number}>
                            ۲
                        </div>
                        <div className={style.text}>
                            تکمیل اطلاعات
                        </div>
                    </div>
                    <div className={style.disable}>
                        <div className={style.number}>
                            ۳
                        </div>
                        <div className={style.text}>
                            درگاه پرداخت
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ShopingProgress;