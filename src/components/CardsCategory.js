//#root importation
import React from 'react';

//#styling and components importation
import style from '../scss/components/_CardsCategory.module.scss';

const CardsCategory = (props) => {
    const { className, title } = props
    return (
        <div className={className}>
            <header className={style.cardsHeader}>
                <h2>{title}</h2>
            </header>
            <nav className={style.cardsNavigation}>
                <a href="https://google.com">
                    همه محصولات
                </a>
            </nav>
        </div>
    );
};

export default CardsCategory;